package com.apress.gerber.gradleweather;

import java.util.List;
import java.util.Map;

public interface TemperatureData {
    List<TemperatureItem> getTemperatureItems();

    Map<String, String> getCurrentConditions();

    CharSequence getCity();
}